var exec = require('child_process').exec;
const execSync = require('child_process').execSync;

function execCommandSync(cmd) {
    return execSync(cmd).toString().replace(/ +(?= )/g, '');
}

module.exports = {
    listContainerSync: function () {
        const lines = execCommandSync('sudo docker container list').split("\n");
        const delim = " ";
        var result = [];

        if (lines.length == 1) return [];

        for (var idx = 1; idx < lines.length; idx++) {
            const line = lines[idx];
            const containerID = line.split(delim)[0];
            const containerImage = line.split(delim)[1];

            if (line.trim() == "") continue;

            result.push({
                id: containerID,
                image: containerImage
            });
        }

        return result;
    },

    listImageSync: function () {
        const lines = execCommandSync('sudo docker image list').split("\n");
        const delim = " ";
        var result = [];

        if (lines.length == 1) return [];

        for (var idx = 1; idx < lines.length; idx++) {
            const line = lines[idx];
            const name = line.split(delim)[0];
            const tag = line.split(delim)[1];
            const id = line.split(delim)[2];
            const size = line.split(delim).pop();

            if (line.trim() == "") continue;

            result.push({ name, tag, id, size });
        }

        return result;
    },

    killContainerSync: function (id) {
        execCommandSync(`sudo docker container kill ${id}`).split("\n");
        var found = false;

        this.listContainerSync().forEach(container => {
            if (container.id == id) { found = true; }
        });

        return !found;
    },

    killAllContainersSync: function () {
        const containers = this.listContainerSync();
        containers.forEach(container => {
            execCommandSync(`sudo docker container kill ${container.id}`);
        });
    },

    deleteImageSync: function (id) {
        execCommandSync(`sudo docker image remove -f ${id}`).split("\n");
        var found = false;

        this.listImageSync().forEach(image => {
            if (image.id == id) { found = true; }
        });

        return !found;
    },

    startContainer: function (flags, image, callback) {
        return exec(`sudo docker run ${flags} ${image}`, function (error, stdout, stderr) {
            callback(error, stdout, stderr);
        });
    },

    startContainerSync: function (flags, image) {
        return execCommandSync(`sudo docker run -d ${flags} ${image}`);
    },

    buildImageSync: function (name, loc) {
        execCommandSync(`sudo docker build -t ${name} ${loc}`);
        var found = false;

        this.listImageSync().forEach(image => {
            if (image.name == name) { found = true; }
        });

        return found;
    }
}
