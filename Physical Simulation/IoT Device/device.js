const deviceId = 50;
const io = require("socket.io-client");
var ss = require('socket.io-stream');
const terminal = require('./terminal-utili.js');
const sensor = require('./simulate-data.js');
var exec = require('child_process').exec;
const { pipeline } = require('stream');
const yaml = require('yaml');
const fs = require('fs');

//Socket connections
//var gatewaySocket = io("http://localhost:270", { query: `deviceId=${deviceId}&type=IOT-DEVICE`, transports: ['websocket', 'polling']});
var gatewaySocket;
var helperSocket = io("http://localhost:500/", { query: `deviceId=${deviceId}&type=IOT-DEVICE`, transports: ['websocket', 'polling'] });

//Experimnent testing variables
var deviceProperties = require("./props.json");
var report = {type: "report", title: undefined, timings:{start: undefined, end: undefined}};
var latestBuild = undefined;
var attempts = {redeployment: 0, sensor: 0};

function printMenu() {
    terminal.rawPrint(`------ MENU v${deviceProperties.version}------`);
    terminal.rawPrint("/gateway list - List nearby gateways");
    terminal.rawPrint("/gateway connect <address> - Connect to a gateway");
    terminal.print("/gateway disconnect - Disconnect from gateway");
}

function startReportLog(title, start) {
    report.title = title;
    report.timings.start = start;
}

function appendReportLog(title, lap) {
    report.title = title;
    Object.assign(report.timings, lap);
}

function endReportLog(title, end) {
    report.title = title;
    report.timings.end = end;
}

function getIPAddress() {
    var interfaces = require('os').networkInterfaces();
    
    for (var devName in interfaces) {
        var iface = interfaces[devName];

        for (var i = 0; i < iface.length; i++) {
            var alias = iface[i];
            if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal)
                return alias.address;
        }
    }

    return '0.0.0.0';
}

function pingGateway() {
    gatewaySocket.emit("ping_gateway", {data: getIPAddress(), message: `Gateway has been pinged by device "${deviceId}" (current).`});
}

function connectGateway(devices) {
    if (gatewaySocket != undefined) {
        return;
    }
    
    devices.forEach(device => {
        const ip = device.ip;
        const ports = device.ports;
        //terminal.rawPrint(`${ip} || ${JSON.stringify(ports[0])}`);
        
        for (var idx = 0; idx < ports.length; idx++) {
            if (ports[idx].port == 270 && ports[idx].state == "open") {
                gatewaySocket = io(`http://${ip}:${ports[idx].port}`, {
                    query: `deviceId=${deviceId}&type=IOT-DEVICE`,
                    transports: ['websocket', 'polling']                    
                });
                terminal.print(`Found gateway at address ${ip}:${ports[idx].port}`);

                break;
            }
        }
    });   

    if (gatewaySocket != undefined) {
        startGatewayEvents();
        terminal.rawPrint(`Pinging gateway...`);
        pingGateway();
        startSensorSimulatiuon();
        helperSocket.emit("request_buffered_message", true);
    }
}

function disconnectGateway() {
    //gatewaySocket.disconnect();
    //gatewaySocket = undefined;
    terminal.print("Disconnected from gateway.");
}

function startSensorSimulatiuon() {
    const intervalId = setInterval(() => {
        if (gatewaySocket == undefined) {
            clearInterval(intervalId);
            return;
        }
        
        //Failure rate (5%)
        if (Math.random() < 0.05){
            deviceProperties.disableDatastream = true;
            startReportLog("SensorFailure", Date.now());
        }
        
        if (!deviceProperties.disableDatastream) {
            var data = sensor.fetchSensorData();
            data.deviceId = deviceId;
            gatewaySocket.emit("device_data_stream", data);
        }
    },  deviceProperties.sensorTimer);

    return intervalId;
}

/// Gateway Events
function startGatewayEvents() {
    ss(gatewaySocket).on('stream_device_redeployment', function(stream, data) {
        const filename = `${process.cwd()}/load.txt`;
        const writeStream = fs.createWriteStream(filename);
        latestBuild = data || {};
        latestBuild.awaitStream = true;

        // pipeline(stream, process.stdout, err => {
        //     console.log(err);
        // });

        writeStream.on('pipe', function(data) {
            terminal.print("Data is being piped!");                
        });
        
        stream.pipe(writeStream);
        console.log(latestBuild);

        stream.on('end', function(data) {
            terminal.print("Stream end!"); 
        });

        writeStream.on('end', function(data) {
            terminal.print("Stream end!"); 
        });
    });

    gatewaySocket.on("ping_gateway", (data) => {
        terminal.print(data);
    });

    gatewaySocket.on("request_device_reconfigure", function (data) {
        if (!deviceProperties.hasOwnProperty(data.key)) {
            gatewaySocket.emit("response_reconfigure", {success: false});
            return;
        }

        //Change type of value depening on key
        if (data.key == "sensorTimer") {
            data.value = parseInt(data.value);
        }

        gatewaySocket.emit("response_reconfigure", {success: true});
        terminal.rawPrint(`Properties modified @ ${new Date().toLocaleString()}`);
        terminal.rawPrint("------------------------------------------");
        terminal.rawPrint("Device properties has been modified (OLD)");
        terminal.print(deviceProperties);
        terminal.rawPrint("Device properties has been modified (NEW)");
        deviceProperties[data.key] = data.value;
        terminal.print(deviceProperties);
        
    });
    
    gatewaySocket.on("request_device_redeployment", (data) => {
        //Forward request to helper process
        helperSocket.emit("request_device_redeployment", data);
        terminal.print("Forwarded redployment request to helper process.");
        latestBuild = data;
    });

    gatewaySocket.on("disconnect", () => {
        disconnectGateway();
    });
    
    gatewaySocket.on("reload_sensor", (data) => {
        //sensor reload
        if (Math.random() < 0.15) {
            deviceProperties.disableDatastream = false;
            endReportLog("SensorFailure", Date.now());
            gatewaySocket.emit("misc_message", {type: "REPORT", report: report});
        }
    });
}

/// Helper Process Events
function startHelperEvents() {
    helperSocket.on("scan_network", (data) => {
        terminal.print(data);
        connectGateway(data);
    });
    
    helperSocket.on("response_redeployment", (data) => {
        if (data.success) {
            const imgInfo = require("./image-info.json");
            data.size = imgInfo.size;
            gatewaySocket.emit("response_redeployment", data);
        }
        else {
            //Forward request to helper process again becuase it failed (3 TRIES)
            if (attempts.redeployment < 3) {
                helperSocket.emit("request_device_redeployment", latestBuild);
                attempts.redeployment += 1;
                terminal.print(`IoT Device has tried ${attempts.redeployment} attempts at updating.`);
            }
            else {
                gatewaySocket.emit("response_redeployment", data);
            }
        }
    });

    helperSocket.on("request_device_redeployment", function (data) {
        helperSocket.emit("request_device_redeployment", data);
    });

    helperSocket.on("disconnect", () => {
        //disconnectGateway();
    });
}

/// Terminal Stuff (Manual Mode)
if (deviceProperties.autonomous == false) {
    printMenu();
    terminal.onLineInput(line => {
        const args = line.toLowerCase().split(' ');

        if (args[0] == "?") {
            printMenu();
        }
        else if (args[0] == "gateway" && args[1] == "list") {
            terminal.print("Requesting for network scan for nearby gateways...");
	        helperSocket.emit("scan_network", true);
        } 
        else if (args[0] == "gateway" && args[1] == "connect" && args[2] != undefined) {
            if (gatewaySocket != undefined) {
                terminal.print("You must first disconnect from the current gateway.");
                return;
            }

            connectGateway(args[2]);
        } 
        else if (args[0] == "gateway" && args[1] == "disconnect") {
            if (gatewaySocket == undefined) {
                terminal.print("You must first connect to an active gateway.");
                return;
            }

            disconnectGateway();
        } 
        else {
            terminal.clearPreviousLine();
        }
    });
}

//Attempt to connect to gateway 
startHelperEvents();
setInterval(() => {
    if (gatewaySocket == undefined) {
        //Scan for gateways
        terminal.rawPrint("Scanning for gateways...");
        helperSocket.emit("scan_network", true);
    }
}, 10 * 1000);

if (gatewaySocket != undefined) {
    startGatewayEvents();
    pingGateway();
}