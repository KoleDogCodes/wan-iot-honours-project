const deviceId = -1;
//Import Libraires 
var app = require('express')();
const cors = require('cors');
const server = require('http').Server(app);
const io = require('socket.io')(server, { maxHttpBufferSize: 1500000000 });
var ss = require('socket.io-stream');
var exec = require('child_process').exec;
const execSync = require('child_process').execSync;
const fs = require('fs');
const terminal = require('./terminal-utili.js');
const yaml = require('yaml');
const docker = require("./docker-manager.js");
var bufferedMessage = undefined;
var start = undefined;
var end = undefined;
const buildErrorChance = 0;
var latestBuild = undefined;

app.use(cors());
server.listen(500);
terminal.print(`Build Error Chance is: ${buildErrorChance * 100}%`);

///File functions
function createLocalDirectory(path) {
    try {
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }

        return true;
    }
    catch (err) {
        terminal.print(`ERROR: Failed to create directory at "${path}"!`);
        return false;
    }
}

function writeLocalFile(path, contents) {
    try {
        fs.writeFileSync(`${process.cwd()}/${path}`, contents);
        return true;
    }
    catch (err) {
        terminal.print(`ERROR: Failed to overwrite contents to file at "${path}"!`);
        return false;
    }
}

function appendLocalFile(path, contents) {
    try {
        fs.appendFileSync(`${process.cwd()}/${path}`, contents);
        return true;
    }
    catch (err) {
        terminal.print(`ERROR: Failed to append contents to file at "${path}"!`);
        return false;
    }
}

function deleteLocalFile(path) {
    try {
        fs.unlinkSync(`${process.cwd()}/${path}`);
        return true;
    }
    catch (err) {
        terminal.print(`ERROR: Failed to delete file at "${path}"!`);
        return false;
    }
}

function splitOnce(str, split) {
    var components = str.split(split);
    return [components.shift(), components.join(split)];
}

function runBuild(buildData, onError, onSuccess) {
    const build = yaml.parse(buildData["build.yml"]);
    const actions = build.actions;
    var currentPath = "";
    var result = true;
    var idx = 0;

    for (idx = 0; idx < actions.length; idx++) {
        const action = splitOnce(actions[idx], " ")[0];
        const filename = splitOnce(actions[idx], " ")[1];

        //RANDOM CHANCE TO FAIL BUILD
        if (Math.random() < buildErrorChance) {
            result = false;
            break;
        }

        //OVERWRITE action - Overwrite exisitng file
        if (action.startsWith("OVERWRITE")) {
            result = writeLocalFile(currentPath + filename, buildData[filename]);
            if (!result) break;
        }

        //MKDIR action - Make directory
        else if (action.startsWith("MKDIR")) {
            result = createLocalDirectory(filename);
            if (!result) break;
        }

        //DELETE action - Delete file
        else if (action.startsWith("DELETE")) {
            result = deleteLocalFile(filename);
            if (!result) break;
        }

        //REBUILD action - Rebuild
        else if (action.startsWith("REBUILD")) {
            break;
        }
    }

    if (result) {
        onSuccess();
    }
    else {
        onError(actions[idx]);
    }
}

///Network Functions
function getIPAddress() {
    var interfaces = require('os').networkInterfaces();

    for (var devName in interfaces) {
        var iface = interfaces[devName];

        for (var i = 0; i < iface.length; i++) {
            var alias = iface[i];
            if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal)
                return alias.address;
        }
    }

    return '0.0.0.0';
}

function scanNetwork(callback) {
    var info = { ip: null, ports: [] };
    var results = [];
    var portFlag = false;

    exec(`nmap -p 270 ${getIPAddress()}/24`, function (error, stdout, stderr) {
        const lines = stdout.split("\n");

        lines.forEach(line => {
            //Push whatever data to end of array
            if (line.startsWith("Nmap scan report for ") && info.ip != null) {
                results.push({ ip: info.ip, ports: info.ports });
                info.ip = null;
                info.ports = [];
                portFlag = false;
            }

            if (line.startsWith("Nmap done:")) {
                results.push({ ip: info.ip, ports: info.ports });
                callback(results);
            }

            //Grab ip data
            else if (line.startsWith("Nmap scan report for ") && info.ip == null) {
                info.ip = line.split("Nmap scan report for ")[1];
            }

            //Set port flag
            else if (line.startsWith("PORT")) {
                portFlag = true;
            }

            else if (portFlag) {
                //terminal.print(`${info.ip} && ${line}`);
                const portData = line.split(" ");
                if (portData.length >= 3) {
                    var portInfo = {
                        port: parseInt(portData[0].split("/")[0]),
                        protocol: portData[0].split("/")[1],
                        state: portData[1]
                    };
                    //terminal.print(`${info.ip} && ${JSON.stringify(portData)}`);

                    info.ports.push(portInfo);
                }
            }
        });

        //callback(results); 
    });
}

///Docker Functions
function getOldestDeviceImage() {
    const images = docker.listImageSync();
    var img = undefined;

    images.forEach(image => {
        if (image.name.startsWith("node-device-device-v-")) {
            img = image;
        }
    });

    return img;
}

function findDockerContainer(imageName) {
    const containers = docker.listContainerSync();
    var container = undefined;

    containers.forEach(cont => {
        if (cont.image == imageName) {
            container = cont;
        }
    });

    return container;
}

/// Events for the update manager (socket = IOT-DEVICE connection)
io.on("connection", function (socket) {
    ss(socket).on('stream_device_redeployment', function (stream, data) {
        latestBuild = data;
        latestBuild.awaitStream = true;
        const filename = `${process.cwd()}/load.txt`;
        stream.pipe(fs.createWriteStream(filename));
        terminal.rawPrint("Incoming file stream with build data as follows:");
        terminal.print(latestBuild);

        stream.on('end', (data) => {
            io.emit("request_device_redeployment", latestBuild);
            terminal.rawPrint("File stream has ended");
        });
    });

    socket.on('scan_network', function (data) {
        terminal.print("Network scan requested...");
        scanNetwork((result) => {
            socket.emit("scan_network", result);
        });
    });

    socket.on('request_device_redeployment', function (data) {
        terminal.print("IoT device(s) rebuild requested...");
        runBuild(data, (action) => {
            socket.emit("response_redeployment", { success: false, message: `ERROR: Failed to execute action "${action}" from build.yml!` });
        },
            //onSucess
            () => {
                //Get current container id of iot device
                const containerId = findDockerContainer(getOldestDeviceImage().name);
                const imageName = getOldestDeviceImage().name;
                const newVersion = `node-device-device-v-${parseInt(getOldestDeviceImage().name.split("-").pop()) + 1}`;

                //Build new image for updated firmware & Kill old container
                docker.killAllContainersSync();
                start = Date.now();
                docker.buildImageSync(newVersion, `.`);

                var containerCrashed = { result: false, msg: "" };

                //RANDOM CHANCE TO FAIL BUILD
                if (Math.random() < buildErrorChance) {
                    docker.deleteImageSync(newVersion);
                    containerCrashed.result = true;
                }
                else {
                    //Start updated image
                    docker.startContainer(`--network host -v "$(pwd):/app"`, newVersion, (err, stdout, stderr) => {
                        if (err != undefined) {
                            containerCrashed.result = true;
                            containerCrashed.msg = stderr;
                            console.log(stderr);
                        }
                    });
                }

                //Wait X seconds to see if container has crashed
                setTimeout(() => {
                    if (!containerCrashed.result) {
                        //Remove old image
                        docker.deleteImageSync(imageName);

                        //Update props.json with image data
                        const newImg = getOldestDeviceImage();
                        deleteLocalFile("image-info.json");
                        writeLocalFile("image-info.json", JSON.stringify({ name: newImg.name, size: newImg.size }));
                        end = Date.now();

                        //Wait for device to reconnect then send message
                        bufferedMessage = { success: true, message: `Done`, recoveryTime: (end - start) / 1000 };
                    }
                    else {
                        //Send response back
                        bufferedMessage = { success: false, message: containerCrashed.msg, recoveryTime: (end - start) / 1000 };
                        docker.startContainer(`--network host -v "$(pwd):/app"`, imageName, (err, stdout, stderr) => { });
                        //socket.emit("response_redeployment", { success: false, message: containerCrashed.msg });
                    }
                }, 5000);
            });
    });

    socket.on('request_buffered_message', function (data) {
        if (bufferedMessage != undefined) {
            socket.emit("response_redeployment", bufferedMessage);
            bufferedMessage = undefined;
        }
    });

    socket.on('disconnect', function () {
        const deviceId = socket.handshake.query.deviceId;
        const type = socket.handshake.query.type;

        if (type == "IOT-DEVICE") {
            terminal.print(`IoT device "${deviceId}" has disconnected.`);
        }
    });
});
