const io = require("socket.io-client");
var ss = require('socket.io-stream');
const terminal = require('./terminal-utili.js');
var fs = require('fs');
var chart = require('asciichart');
const yaml = require('yaml');
const openExplorer = require('open-file-explorer');
const { PerformanceObserver, performance } = require('perf_hooks');
var socket = io("http://192.168.0.252:270");
var devices = {};
var dashboardProperties = {
    chartMaxValues: 75,
    chartView: false
};
var chartValues = {
    temp: [],
    bpm: []
};
var startTime, endTime;
const resultFilename = "Exp 1 - IOT DEVICE.json";
var resultSuffix = `//${Math.round(fs.statSync(`${process.cwd()}\\update\\load.txt`).size / (1024*1024))}MB`;

function printMenu() {
    terminal.rawPrint("------ MENU ------");
    terminal.rawPrint("/devices - List all connected iot devices within the network");
    terminal.rawPrint("/dashboard - Display the dashboard");
    terminal.rawPrint("/redeploy <edit/run> - Redeploy and build docker image for all iot devices");
    terminal.print("/reconfigure <D|G:id> <property> <value> - Reconfigure an iot device or gateway device.");
}

function sendReconfigurationRequest(deviceId, property, value) {
    const eventName = deviceId.startsWith("d") ? "request_device_reconfigure" : "request_gateway_reconfigure";
    const data = {
        deviceId: deviceId.split(":")[1],
        key: property,
        value: value
    };

    socket.emit(eventName, data);
    terminal.rawPrint(`Sending following reconfiguration request ${deviceId} @ ${property}=${value}`);
    startTime = performance.now();
}

function readFiles(dirname, onFileContent, onComplete) {
    fs.readdirSync(dirname).forEach(filename => {
        const data = fs.readFileSync(`${dirname}\\${filename}`, {encoding:'utf8', flag:'r'});
        onFileContent(filename, data);
    });

    onComplete();
}

function writeFile(filename, contents) {
    fs.appendFileSync(`${process.cwd()}\\${filename}`, contents);
}

function validateBuildFile(contents) {
    try  {
        yaml.parse(contents);
        return true;
    }
    catch {
        return false;
    }
}

function openUpdateDirectory() {
    const path = `${process.cwd()}\\update`;
    if (!fs.existsSync(path)){
        fs.mkdirSync(path);
    }

    openExplorer(path, err => {
        if (err) {
            terminal.rawPrint(`ERROR: Could not open to path '${path} please paste files there.'`);
        } 
        else {
            terminal.rawPrint(`Please run 'redeploy deploy' to push changes.`);
        }
    });
}

function pushUpdateDirectory() {
    const path = `${process.cwd()}\\update\\`;
    const updateConfPath = `${path}\\build.yml`;
    var files = {
        "build.yml": undefined
    };

    //Check config file
    if (!fs.existsSync(updateConfPath)){
        terminal.print("ERROR: 'build.yml' file does not exist.");
        return;
    }
    
    //onFileContent    
    readFiles(path, (filename, text) => {
        files[filename] = text;
    }, 

    //onComplete
    () => {
        if (!validateBuildFile(files["build.yml"])) {
            terminal.print("Invalid build.yml please check the formatting.");
            return;
        }

        terminal.print(files["build.yml"]);
        terminal.print("Build is being pushed to IoT gateways");
        startTime = performance.now();
        socket.emit("request_device_redeployment", files);
    });   
}

/// Backend Events
socket.on("update_device_status", function(data) {
    const { gatewayId, deviceId, status } = data;
    
    if (!(gatewayId in devices) || devices[gatewayId] == undefined) {
        devices[gatewayId] = [];
    }

    //Device connected to gateway X
    if (gatewayId != undefined && deviceId != undefined && status == "online") {
        devices[gatewayId].push(deviceId);
    }
    else if (gatewayId != undefined && deviceId != undefined && status == "offline") {
        devices[gatewayId] = devices[gatewayId].filter(item => item !== deviceId);
    }
});

socket.on("dashboard_data_stream", (data) => {
    if (chartValues.temp.length >= dashboardProperties.chartMaxValues) {
        chartValues.temp.shift();
    }

    if (chartValues.bpm.length >= dashboardProperties.chartMaxValues) {
        chartValues.bpm.shift();
    }

    chartValues.temp.push(data.temp);
    chartValues.bpm.push(data.bpm);

    if (dashboardProperties.chartView) {
        terminal.updatePrint("chart.temp", chart.plot(chartValues.temp));
        terminal.updatePrint("chart.bpm", chart.plot(chartValues.bpm));
    }
});

socket.on("response_reconfigure", (data) => {
    endTime = performance.now();
    const elapsedTime = ((endTime - startTime) / 1000).toFixed(2);
    if (!data.success) {
        terminal.print(`Failed to reconfigure property value | Elapsed Time: ${elapsedTime}s`);
    } 
    else {
        terminal.print(`Successfully reconfigured property value | Elapsed Time: ${elapsedTime}s`);
    }
});

socket.on("request_device_status", (data) => {
    for (const gateway in data) {
        const deviceIds = data[gateway];
        terminal.rawPrint(`Gateway ID "${gateway}" has the following devices:`);
        
        //No connected devices
        if (deviceIds.length == 0) {
            terminal.rawPrint(`No connected devices`);
            terminal.print();
            continue;
        }

        //Print device ids
        deviceIds.forEach(id => {
            terminal.rawPrint(`- Device ${id}`);
        });

        terminal.print();
    }
});

socket.on("misc_message", (data) => {
    if (data.type == "REPORT") {
        const report = data.report;
        const timings = report.timings;
        const recoveryTime = ((timings.end - timings.start) / 1000).toFixed(2);
        //terminal.print(`REPORT ${report.title} | Timings: ${recoveryTime}`);
        //writeFile(resultFilename, `${recoveryTime}\n`);
    }
});

socket.on("response_redeployment", (data) => {
    endTime = performance.now();
    const elapsedTime = ((endTime - startTime) / 1000).toFixed(2);
    if (!data.success) {
        terminal.rawPrint(`Failed to redeploy | Elapsed Time: ${elapsedTime}s`);
        terminal.rawPrint(`######### BEGIN ERROR REPORT #########`);
        terminal.rawPrint(`${data.message}`);
        terminal.print(`######### END ERROR REPORT #########`);
    } 
    else {
        terminal.rawPrint(data);
        terminal.print(`Successfully redeployed | Elapsed Time: ${elapsedTime}s`);
        writeFile(resultFilename, 
            `${JSON.stringify({
                size: parseInt(data.size.replace("MB", "")), 
                dataLoss: data.recoveryTime, 
                elapsedTime: elapsedTime,
                attempts: 1
            }, null, 2)},${resultSuffix}\n`);
    }
});

/// Terminal Stuff
printMenu();
terminal.onLineInput(line => {
    const args = line.toLowerCase().split(' ');
    const rawArgs = line.split(' ');

    if (args[0] == "?") {
        printMenu();
    } 
    else if (args[0] == "dashboard") {
        dashboardProperties.chartView = true;
        terminal.rawPrint("Chart flow mode on..");
        terminal.rawPrint("(ctrl + r) - Exit chart viewing mode");
        terminal.pauseReadLine();

        terminal.tagPrint("chart.temp.label", "Temp Chart");
        terminal.tagPrint("chart.temp", "Generating chart...");
        terminal.tagPrint("chart.bpm.label", "Beats per minute Chart");
        terminal.tagPrint("chart.bpm", "Generating chart...");
    } 
    else if (args[0] == "devices") {
        terminal.rawPrint("---------- Device List ----------");
        socket.emit("request_device_status", true);
    } 
    else if (args[0] == "redeploy" && args[1] == "edit") {
        openUpdateDirectory();
        terminal.print(`Please run 'redeploy deploy' after pasting files to push changes.`);
    } 
    else if (args[0] == "redeploy" && args[1] == "deploy") {
        pushUpdateDirectory();
    } 
    else if (args[0] == "reconfigure" && args.length == 4) {
        const targetDevice = args[1];
        const propertyName = rawArgs[2]
        const propertyValue = rawArgs[3];
        sendReconfigurationRequest(targetDevice, propertyName, propertyValue);
    } 
    else {
        terminal.clearPreviousLine();
    }
});

// terminal.onKeyPress((event) => {
//     //ctrl + r
//     if ((event.ctrlKey && event.rawcode == 82) && dashboardProperties.chartView) {
//         dashboardProperties.chartView = false;
//         terminal.clearTagPrint("chart.temp");
//         terminal.clearTagPrint("chart.bpm");
//         terminal.clearTagPrint("chart.temp.label");
//         terminal.clearTagPrint("chart.bpm.label");
//         terminal.print("Chart flow mode is now turned off...");
//         terminal.resumeReadLine();
//     }
// });