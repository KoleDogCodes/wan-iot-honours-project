const prompt = require('prompt-sync')();
const readline = require('readline');
var Table = require('cli-table');
// const ioHook = require('iohook');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
var lines = [];

module.exports = {
    clearPreviousLine: function () {
        process.stdout.moveCursor(0, -1);
        process.stdout.clearLine(0);
    },

    clearScreen: function () {
        process.stdout.write('\x1b[H\x1b[2J');
    },

    printScreen: function () {
        this.clearScreen();
        lines.forEach(element => {
            if (element.data == undefined || element == "") {
                console.log();
            } else {
                console.log(element.data);
            }
        });
    },

    updatePrint: function (id, line) {
        for (var index = 0; index < lines.length; index++) {
            if (lines[index].hasOwnProperty('id') == false) { continue; }

            if (lines[index].id == id) {
                lines[index] = { id: id, data: line };
                this.printScreen();
            }
        }
    },

    clearTagPrint: function (id) {
        for (var index = 0; index < lines.length; index++) {
            if (lines[index] != undefined && lines[index].id == id) {
                delete lines[index];
            }
        }

        this.clearScreen();
        this.printScreen();
    },

    rawPrint: function (line) {
        if (line == undefined) line = " ";
        lines.push({ id: "stdout", data: line });
        console.log(line);
    },

    print: function (line) {
        if (line == undefined) line = " ";
        lines.push({ id: "stdout", data: line });
        lines.push(" ");
        console.log(line);
        console.log();
    },

    tagPrint: function (id, line) {
        lines.push({ id: id, data: line });
        lines.push(" ");
        console.log(line);
        console.log();
    },

    getInput: function (question) {
        return prompt(question);
    },

    onLineInput: function (callback) {
        rl.on('line', data => {
            lines.push({ id: "readline", data: data });
            callback(data);
        });
    },

    // onKeyPress: function (callback) {
    //     ioHook.on("keydown", event => {
    //         callback(event);
    //         // console.log(event);
    //     });
    // },

    pauseReadLine: function () {
        rl.pause();
    },

    resumeReadLine: function () {
        rl.resume();
    },

    generateTable: function (headers, colWidths, values) {
        var table = new Table({
            head: headers,
            colWidths: colWidths
        });

        values.forEach(element => {
            table.push(element);
        });
        return table.toString();
    }
}

// ioHook.start();