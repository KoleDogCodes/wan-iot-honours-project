const deviceId = -1;
//Import Libraires 
var app = require('express')();
const cors = require('cors');
const server = require('http').Server(app);
const io = require('socket.io')(server, {maxHttpBufferSize: 1500000000});
var ss = require('socket.io-stream');
var exec = require('child_process').exec;
const execSync = require('child_process').execSync;
const fs = require('fs');
const terminal = require('./terminal-utili.js');
const yaml = require('yaml');
const docker = require("./docker-manager.js");
var bufferedMessage = undefined;
var start = undefined;
var end = undefined;
const buildErrorChance = 0;
var latestBuild = undefined;

app.use(cors());
server.listen(500);

terminal.print(`Build Error Chance: ${buildErrorChance * 100}%`);

///File functions
function createLocalDirectory(path) {
    try {
        if (!fs.existsSync(path)){
            fs.mkdirSync(path);
        }   
        
        return true;
    } 
    catch(err) {
        terminal.print(`ERROR: Failed to create directory at "${path}"!`);
        return false;
    }
}

function writeLocalFile(path, contents) {
    try {
        fs.writeFileSync(`${process.cwd()}/${path}`, contents);
        return true;
    } 
    catch(err) {
        terminal.print(`ERROR: Failed to overwrite contents to file at "${path}"!`);
        return false;
    }
}

function appendLocalFile(path, contents) {
    try {
        fs.appendFileSync(`${process.cwd()}/${path}`, contents);
        return true;
    } 
    catch(err) {
        terminal.print(`ERROR: Failed to append contents to file at "${path}"!`);
        return false;
    }
}

function deleteLocalFile(path) {
    try {
        fs.unlinkSync(`${process.cwd()}/${path}`);
        return true;
    } 
    catch(err) {
        terminal.print(`ERROR: Failed to delete file at "${path}"!`);
        return false;
    }
}

function splitOnce(str, split) {
    var components = str.split(split);
    return [components.shift(), components.join(split)];
}

function runBuild(buildData, onError, onSuccess) {
    if (buildData == undefined) {
        onError("build.yml is NULL");
        return;
    }
    
    const build = yaml.parse(buildData["build.yml"]);
    const actions = build.actions;
    var currentPath = "";
    var result = true;
    var idx = 0;

    for (idx = 0; idx < actions.length; idx++) {
        const action = splitOnce(actions[idx], " ")[0];
        const filename = splitOnce(actions[idx], " ")[1];
        
        //RANDOM CHANCE TO FAIL BUILD
        if (Math.random() < buildErrorChance) {
            result = false;
            break; 
        }
        
        //OVERWRITE action - Overwrite exisitng file
        if (action.startsWith("OVERWRITE")) {
            result = writeLocalFile(currentPath + filename, buildData[filename]);
            if (!result) break;
        }
        
        //MKDIR action - Make directory
        else if (action.startsWith("MKDIR")) {
            result = createLocalDirectory(filename);
            if (!result) break;
        }
        
        //DELETE action - Delete file
        else if (action.startsWith("DELETE")) {
            result = deleteLocalFile(filename);
            if (!result) break;
        }
        
        //REBUILD action - Rebuild directory
        else if (action.startsWith("REBUILD")) {
            break;
        }
    }
    
    if (result) {
        onSuccess();
    }
    else {
        onError(actions[idx]);
    }   
}

///Docker Functions
function getOldestDeviceImage() {
    const images = docker.listImageSync();
    var img = undefined;
    
    images.forEach(image => {
        if (image.name.startsWith("node-device-gateway-v-")) {
            img = image;
        }
    });
    
    return img;
}

function findDockerContainer(imageName) {
    const containers = docker.listContainerSync();
    var container = undefined;
    
    containers.forEach(cont => {
        if (cont.image == imageName) {
            container = cont;
        }
    });
    
    return container;
}
    
/// Events for the update manager (socket = IOT-GATEWAY connection)
io.on("connection", function (socket) {
    
    socket.emit("connected", true);

    ss(socket).on('stream_device_redeployment', function(stream, data) {
        latestBuild = data;
        latestBuild.awaitStream = true;
        const filename = `${process.cwd()}/load.txt`;
        stream.pipe(fs.createWriteStream(filename));
        terminal.rawPrint("Incoming file stream with build data as follows:");
        terminal.print(latestBuild);

        stream.on('end', (data) => {
            io.emit("request_device_redeployment", latestBuild);
            terminal.rawPrint("File stream has ended");
        });
    });
	
    socket.on('request_device_redeployment', function (data) {
        terminal.print("IoT gateway(s) rebuild requested...");
        runBuild(data, (action) => {
            socket.emit("response_redeployment", {success: false, message: `ERROR: Failed to execute action "${action}" from build.yml!`});
        },
        //onSucess
        () => {
            //Get current container id of iot device
            const containerId = findDockerContainer(getOldestDeviceImage().name);
            const imageName = getOldestDeviceImage().name;
            const newVersion = `node-device-gateway-v-${parseInt(getOldestDeviceImage().name.split("-").pop()) + 1}`;
            
            //Build new image for updated firmware && Kill old container
            docker.killAllContainersSync();
            start = Date.now();
            docker.buildImageSync(newVersion, `.`);
            
            var containerCrashed = {result: false, msg: ""};
            
            //RANDOM CHANCE TO FAIL BUILD
            if (Math.random() < buildErrorChance) {
                docker.deleteImageSync(newVersion);
                containerCrashed.result = true;
            }
            else {                
                //Start updated image
                docker.startContainer(`--network host -v "$(pwd):/app"`, newVersion, (err, stdout, stderr) => { 
                    if (err != undefined) {
                        containerCrashed.result = true;
                        containerCrashed.msg = stderr;
                        console.log("error");
                    }
                    
                });                
            }

            //Wait X seconds to see if container has crashed
            setTimeout(() => {
                if (!containerCrashed.result) {
                    //Remove old image
                    docker.deleteImageSync(imageName);
                    
                    //Update info.json with image data
                    const newImg = getOldestDeviceImage();
                    deleteLocalFile("image-info.json");
                    writeLocalFile("image-info.json", JSON.stringify({name: newImg.name, size: newImg.size}));
                    end = Date.now();
                    
                    //Wait for device to reconnect then send message
                    bufferedMessage = {success: true, message: `Done`, recoveryTime: (end - start) / 1000};
                    socket.emit("response_redeployment", bufferedMessage);
                    terminal.print("Sent repsonse back to iot gateway from helper process");
                }
                else {
                    //Retsart old container because new one crashed
                    bufferedMessage = {success: false, message: containerCrashed.msg, recoveryTime: (end - start) / 1000};
                    docker.startContainerSync(`--network host -v "$(pwd):/app"`, imageName);
                    //socket.emit("response_redeployment", {success: false, message: containerCrashed.msg});
                }
            }, 5000);
        });
    });

    socket.on('request_buffered_message', function (data) {
        terminal.print("Requesting for any buffered messages...");
        if (bufferedMessage != undefined) {
            terminal.print("Sending buffered message");
            socket.emit("response_redeployment", bufferedMessage);
            bufferedMessage = undefined;
        }
    });

    socket.on('disconnect', function () {
        const deviceId = socket.handshake.query.deviceId;
        const type = socket.handshake.query.type;

        if (type == "GATEWAY") {
            terminal.print(`IoT gateway "${deviceId}" has disconnected.`);
        }
    });
});
