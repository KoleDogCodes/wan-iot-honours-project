//Import Libraires 
var app = require('express')();
const cors = require('cors');
const server = require('http').Server(app);
const io = require('socket.io')(server, {maxHttpBufferSize: 1500000000});
const clientIo = require("socket.io-client");
var ss = require('socket.io-stream');
const backendIo = require("socket.io-client");
const helperIo = require("socket.io-client");
const { pipeline } = require('stream');
const yaml = require('yaml');
const fs = require('fs');
const port = 270;
const gatewayId = getPsudeoRandom(3);
const backendSocket = backendIo("https://iot-backend-server.herokuapp.com/", { query: `deviceId=${gatewayId}&type=GATEWAY`,  transports: ['websocket', 'polling'] });
var helperSocket = helperIo("http://localhost:500/", { query: `deviceId=${gatewayId}&type=GATEWAY`,  transports: ['websocket', 'polling'] });
const terminal = require('./terminal-utili.js');
var deviceList = [];
var deviceProperties = {
    debug: false,
    deviceTimeout: 5000
};

//Experiment testing variables
var report = {type: "report", title: undefined, timings:{start: undefined, end: undefined}};
var latestBuild = undefined;
var attempts = {redeployment: 0, sensor: 0};
var subStream = ss.createStream();

app.use(cors());
server.listen(port);

function writeLocalFile(path, contents) {
    try {
        fs.writeFileSync(`${process.cwd()}/${path}`, contents);
        return true;
    } 
    catch(err) {
        terminal.print(`ERROR: Failed to overwrite contents to file at "${path}"!`);
        return false;
    }
}

function deleteLocalFile(path) {
    try {
        fs.unlinkSync(`${process.cwd()}/${path}`);
        return true;
    } 
    catch(err) {
        return false;
    }
}

function getPsudeoRandom(length) {
    return parseInt(Date.now().toString().substring(Date.now().toString().length - length));
}

function getIPAddress() {
    var interfaces = require('os').networkInterfaces();
    
    for (var devName in interfaces) {
        var iface = interfaces[devName];

        for (var i = 0; i < iface.length; i++) {
            var alias = iface[i];
            if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal)
                return alias.address;
        }
    }

    return '0.0.0.0';
}

function printMenu() {
    terminal.rawPrint(`------ MENU (${getIPAddress()}:${port}) ------`);
    terminal.rawPrint("/device list - List all connected devices");
    terminal.rawPrint("/device ping <id> - Ping a device");
    terminal.print("/device debug - Display incoming messages from all devices");
}

function getDevice(id) {
    var foundDevice = undefined;

    deviceList.forEach(device => {
        if (device.id == id) {
            foundDevice = device;
        }
    });

    return foundDevice;
}

function updateDevice(id, currentTime) {
    var device = getDevice(id);

    if (device == undefined) { return; }

    const idx = deviceList.indexOf(device);
    device.lastSeen = currentTime;
    deviceList[idx] = device;
}

function updateDeviceState(id, state) {
    var device = getDevice(id);
    const idx = deviceList.indexOf(device);
    device.state = state;
    deviceList[idx] = device;
}

function removeDevice(id) {
    const device = getDevice(id);
    const idx = deviceList.indexOf(device);
    deviceList.splice(idx, 1);
    backendSocket.emit("update_device_status", {deviceId: id, gatewayId, status: "offline"});
}

function cacheDeviceData() {
    var deviceCache = {};
    
    deviceList.forEach(device => {
        deviceCache[device.id] = device.lastSeen;
    });
    
    deleteLocalFile("cache.json");
    writeLocalFile("cache.json", JSON.stringify(deviceCache));
}

function messageDevice(id, room, message) {
    const device = getDevice(id);

    if (device == undefined) {
        terminal.print(`Device "${id}" is not connected to this gateway.`);
        return false;
    }

    io.to(device.socketId).emit(room, message);
    return true;
}

function streamToDevice(id, room, stream, data, fileDir) {
    const device = getDevice(id);

    if (device == undefined) {
        terminal.print(`Device "${id}" is not connected to this gateway.`);
        return false;
    }

    ss(io.to(device.socketId)).emit(room, stream, data);
    fs.createReadStream(fileDir).pipe(stream);
}

function streamData() {
    const buildImageDirectory = `${process.cwd()}/load.txt`;
    const fileSize = fs.statSync(`${buildImageDirectory}`).size;
    
    //Send file to iot device
    const buildProp = yaml.parse(latestBuild["build.yml"]);
    const target = buildProp.target;
    //terminal.print(`Streaming redeployment request for device type "${target.type}" and targets "${target.group}" devices with size (${fileSize} bytes).`);
    cacheDeviceData();
    
    //Target all iot devices
    if (target.type == "iot-device" && target.group == "*") {
        deviceList.forEach(device => {
            var stream = ss.createStream();
            var deviceHelperSocket = clientIo(`http://${device.ip}:500`, {
                query: `deviceId=${gatewayId}&type=GATEWAY`,
                transports: ['websocket', 'polling']                    
            });

            ss(deviceHelperSocket).emit('stream_device_redeployment', stream, latestBuild);
            fs.createReadStream(buildImageDirectory).pipe(stream);
            terminal.print(`Streaming redeployment request for device type "${target.type}" and targets "${target.group}" devices with size (${fileSize} bytes) @ ${device.ip}.`);
        });
    }
    //Target spefic iot device
    else if (target.type == "iot-device" && target.group != "*") {
        var stream = ss.createStream();
        streamToDevice(target.group, 'stream_device_redeployment', stream, latestBuild, buildImageDirectory);
        terminal.print(`Streaming redeployment request for device type "${target.type}" and targets "${target.group}" devices with size (${fileSize} bytes).`);
    }
    //Target spefic gateway
    else if (target.type == "iot-gateway" && target.group == gatewayId) {
        helperSocket.emit("request_device_redeployment", latestBuild);
        terminal.print("Forwarded redployment request to helper process.");      
    }
    //Target all gateway
    else if (target.type == "iot-gateway" && target.group == "*") {
        helperSocket.emit("request_device_redeployment", latestBuild);
        terminal.print("Forwarded redployment request to helper process.");        
    }
}

/// Events for the gateway
io.on("connection", function (socket) {
    ss(socket).on('stream_device_redeployment', function(stream, data) {
        const filename = `${process.cwd()}/load.txt`;
        const writeStream = fs.createWriteStream(filename);
        latestBuild = data;
        latestBuild.awaitStream = true;

        writeStream.on('pipe', function(data) {
            console.log("Data is being piped!");
        });

        stream.pipe(writeStream);
        console.log(latestBuild);

        stream.on('end', function(data) {
            streamData();
        });
    });
    
    socket.on('ping_gateway', function (data) {
        const deviceId = socket.handshake.query.deviceId;
        const type = socket.handshake.query.type;

        if (type == "IOT-DEVICE" && getDevice(deviceId) == undefined) {
            deviceList.push({ id: deviceId, socketId: socket.id, lastSeen: Date.now(), sensorFailure: false, state: "RUNNING", ip: data.data });
            cacheDeviceData();
            socket.broadcast.emit("update_device_status", {deviceId, gatewayId, status: "online"});
            
            terminal.print("Requesting for any buffered messages from helper process...");
            helperSocket.emit("request_buffered_message", true);
        }
        
        terminal.print(`RECIEVED: ${JSON.stringify(data)}`);

        socket.emit("ping_gateway", data.message);
    });

    socket.on('device_data_stream', function (data) {
        const deviceId = socket.handshake.query.deviceId;
        const type = socket.handshake.query.type;

        if (deviceProperties.debug) {
            terminal.print(`Device "${deviceId}" said: ${JSON.stringify(data)}`);
        }

        socket.broadcast.emit("device_data_stream", data);
        updateDevice(deviceId, Date.now());
        cacheDeviceData();
    });

    socket.on('response_reconfigure', function (data) {
        socket.broadcast.emit("response_reconfigure", data);
    });
    
    socket.on('response_redeployment', function (data) {
        socket.broadcast.emit("response_redeployment", data);
    });
    
    socket.on('misc_message', function (data) {
        socket.broadcast.emit("misc_message", data);
    });

    socket.on("request_device_reconfigure", function (data) {
        terminal.rawPrint("Received IoT device reconfiguration request...");
    
        deviceList.forEach(device => {
            if (device.id == data.deviceId) {
                io.to(device.socketId).emit('request_device_reconfigure', data);
                terminal.print("Forwarding IoT device reconfiguration request...");
                return;
            }
        });
    });

    socket.on("request_gateway_reconfigure", function (data) {
        if (!deviceProperties.hasOwnProperty(data.key)) {
            socket.emit("response_reconfigure", {success: false});
            return;
        }
    
        deviceProperties[data.key] = data.value;
        socket.emit("response_reconfigure", {success: true});
    });
    
    socket.on("request_device_redeployment", function (data) {
        const buildProp = yaml.parse(data["build.yml"]);
        const target = buildProp.target;
        latestBuild = data;
        terminal.print(`Redeployment request for device type "${target.type}" and targets "${target.group}" devices.`);
        cacheDeviceData();
        
        //Target all iot devices
        if (target.type == "iot-device" && target.group == "*") {
            io.emit("request_device_redeployment", data);
        }
        //Target spefic iot device
        else if (target.type == "iot-device" && target.group != "*") {
            messageDevice(target.group, "request_device_redeployment", data);
        }
        //Target spefic gateway
        else if (target.type == "iot-gateway" && target.group == gatewayId) {
            helperSocket.emit("request_device_redeployment", data);
            terminal.print("Forwarded redployment request to helper process.");        
        }
        //Target all gateway
        else if (target.type == "iot-gateway" && target.group == "*") {
            helperSocket.emit("request_device_redeployment", data);
            terminal.print("Forwarded redployment request to helper process.");        
        }
    });

    socket.on('disconnect', function () {
        const deviceId = socket.handshake.query.deviceId;
        const type = socket.handshake.query.type;

        if (type == "IOT-DEVICE") {
            removeDevice(deviceId);
            terminal.print(`IoT device "${deviceId}" has disconnected.`);
        }
    });
});

/// Helper Process Events
helperSocket.on("response_redeployment", (data) => {
    if (data.success) {
        const imgInfo = require("./image-info.json");
        data.size = imgInfo.size;
        io.sockets.emit("response_redeployment", data);
        terminal.print(`IoT Gateway has updated w/ response: ${JSON.stringify(data)}`);
    }
    else {
        //Forward request to helper process again becuase it failed
        helperSocket.emit("request_device_redeployment", latestBuild);
        attempts.redeployment += 1;
        terminal.print(`IoT Gateway has tried ${attempts.redeployment} attempts at updating.`);
    }
});

helperSocket.on("connect", () => {
    terminal.print("Requesting for any buffered messages from helper process...");
    helperSocket.emit("request_buffered_message", true);
});

helperSocket.on("disconnect", () => {
    //disconnectGateway();
});

helperSocket.on("request_device_redeployment", function (data) {
    helperSocket.emit("request_device_redeployment", data);
});

/// Terminal Stuff
printMenu();
terminal.onLineInput(line => {
    const args = line.toLowerCase().split(' ');

    if (args[0] == "?") {
        printMenu();
    }
    else if (args[0] == "device" && args[1] == "list") {
        if (deviceList.length == 0) {
            terminal.print("No connected devices.");

            return;
        }

        const tableHeaders = ['Device ID', 'IP Address', 'Socket Connection ID'];
        const columnWidths = [25, 25, 25];
        const tableValues = [];

        deviceList.forEach(element => {
            var row = [];
            row.push(element.id);
            row.push(element.ip);
            row.push(element.socketId);
            tableValues.push(row);
        });

        terminal.tagPrint("device.list", terminal.generateTable(tableHeaders, columnWidths, tableValues));

    }
    else if (args[0] == "device" && args[1] == "ping" && args[2] != undefined) {
        const result = messageDevice(args[2], "ping_gateway", `Device "${args[2]}" has been pinged by gateway "${gatewayId}".`);
        
        if (result) {
            terminal.print(`Pinging device "${args[2]}"...please check device terminal window.`);
        }
    }
    else if (args[0] == "device" && args[1] == "debug") {
        deviceProperties.debug = true;
        terminal.rawPrint("Debug mode is now turned on.");
        terminal.print("(ctrl + r) - Exit debug mode");
        terminal.pauseReadLine();
    }
    else {
        terminal.clearPreviousLine();
    }
});

//Check for inactive devices
setInterval(() => {
    deviceList.forEach(device => {
        const id = device.id;
        const now = Date.now();
        
        if ((now - device.lastSeen) > deviceProperties.deviceTimeout && device.sensorFailure == false) {
            messageDevice(id, "reload_sensor", false);
            //terminal.print(`Requested device ${id} to reload its sensor(s).`);
        }
        else if ((now - device.lastSeen) > deviceProperties.deviceTimeout && device.sensorFailure) {
            messageDevice(id, "reload_sensor", true);
            //terminal.print(`Requested device ${id} to restart.`);
        }
    });
}, deviceProperties.deviceTimeout);

//Check for buffered messages
setInterval(() => {
    helperSocket.emit("request_buffered_message", true);
}, 1000);
