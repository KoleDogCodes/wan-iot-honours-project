const mongoose = require('mongoose');

module.exports = mongoose.model('SensorData', new mongoose.Schema({
    deviceId: String,
    data: String,
    loggedAt: String
}));