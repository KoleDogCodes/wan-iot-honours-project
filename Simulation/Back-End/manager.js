const sensorData = require('./schemas/sensordata');

module.exports = {
    logSensorData: async function (id, data, loggedAt) {
        const newData = new sensorData({
            deviceId: id,
            data,
            loggedAt
        });

        const promise = newData.save(err => { });
        const promiseValue = await promise;
        return promiseValue;
    }
}