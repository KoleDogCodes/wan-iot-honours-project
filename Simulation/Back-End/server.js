//Import Libraires 
var app = require('express')();
const cors = require('cors');
const server = require('http').Server(app);
const io = require('socket.io')(server, {maxHttpBufferSize: 15000000});
const terminal = require('./terminal-utili.js');
const storage = require('./manager.js');
const mongoose = require('mongoose');

//Variables
const port = process.env.PORT || 3000;
var devices = {};

//Enable cross-origin support and start server
app.use(cors());

//Connect to mongoose
mongoose.connect(`mongodb+srv://admin:P3-nJeJGCu7cB7V@cluster0.kvop1.mongodb.net/WorkerDB?retryWrites=true&w=majority`, { useUnifiedTopology: true, useNewUrlParser: true });

io.on("connection", function(socket) {
    const deviceId = socket.handshake.query.deviceId;
    const type = socket.handshake.query.type;

    if (type == "GATEWAY") {
        devices[deviceId] = [];
    }

    ///Forward device data to dashboard
    socket.on("device_data_stream", async function(data) {
        const { deviceId, temp, bmp } = data;
        await storage.logSensorData(deviceId, {temp, bmp}, Date.now());
        io.emit("dashboard_data_stream", data);
    });

    ///Forward IoT device reconfigruation request to all gateways
    socket.on("request_device_reconfigure", function(data) {
        io.emit("request_device_reconfigure", data);
    });

    ///Update device status
    socket.on("update_device_status", function(data) {
        const { gatewayId, deviceId, status } = data;
        
        if (!(gatewayId in devices) || devices[gatewayId] == undefined) {
            devices[gatewayId] = [];
        }

        //Device connected to gateway X
        if (gatewayId != undefined && deviceId != undefined && status == "online") {
            devices[gatewayId].push(deviceId);
        }
        else if (gatewayId != undefined && deviceId != undefined && status == "offline") {
            devices[gatewayId] = devices[gatewayId].filter(item => item !== deviceId);
        }
    });

    socket.on("request_device_status", function(data) {
        socket.emit("request_device_status", devices);
    });

    ///Forward IoT gateway reconfigruation request to all gateways
    socket.on("request_gateway_reconfigure", function(data) {
        io.emit("request_gateway_reconfigure", data);
    });

    ///Forward IoT device redeployment request to all gateways
    socket.on("request_device_redeployment", function(data) {
        io.emit("request_device_redeployment", data);
    });

    ///Forward reconfigurtaion responses to all active listeners
    socket.on("response_reconfigure", function(data) {
        io.emit("response_reconfigure", data);
    });

    ///Forward redeployment responses to all active listeners
    socket.on("response_redeployment", function(data) {
        io.emit("response_redeployment", data);
    });

    ///Forward reconfigurtaion responses to all active listeners
    socket.on("misc_message", function(data) {
        io.emit("misc_message", data);
    });

    ///Gateway disconnected
    socket.on('disconnect', function() {
        const deviceId = socket.handshake.query.deviceId;
        const type = socket.handshake.query.type;

        if (type == "GATEWAY") {
            terminal.print(`Gateway device "${deviceId}" has disconnected.`);
            devices[deviceId] = undefined;
        } 
		else {
            terminal.print(`Device "${deviceId}" has disconnected.`);
        }

    });
});

server.listen(port);
