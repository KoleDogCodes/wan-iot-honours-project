const io = require("socket.io-client");
const terminal = require('./terminal-utili.js');
const manager = require('./docker-wrapper.js');
var chart = require('asciichart');
const { PerformanceObserver, performance } = require('perf_hooks');
var socket = io("https://iot-backend-server.herokuapp.com/");
var dashboardProperties = {
    chartMaxValues: 75,
    chartView: false
};
var chartValues = {
    temp: [],
    bpm: []
};
var startTime, endTime;

function printMenu() {
    terminal.rawPrint("------ MENU ------");
    terminal.rawPrint("/dashboard - Display the dashboard");
    terminal.rawPrint("/redeploy <path> - Redeploy and build docker image for all iot devices");
    terminal.print("/reconfigure <D|G:id> <property> <value> - Reconfigure an iot device or gateway device. (TODO)");
}

function sendReconfigurationRequest(deviceId, property, value) {
    const eventName = deviceId.startsWith("d") ? "request_device_reconfigure" : "request_gateway_reconfigure";
    const data = {
        deviceId: deviceId.split(":")[1],
        key: property,
        value: value
    };

    socket.emit(eventName, data);
    terminal.rawPrint(`Sending following reconfiguration request ${deviceId} @ ${property}=${value}`);
    startTime = performance.now();
}

/// Backend Events
socket.on("dashboard_data_stream", (data) => {
    if (chartValues.temp.length >= dashboardProperties.chartMaxValues) {
        chartValues.temp.shift();
    }

    if (chartValues.bpm.length >= dashboardProperties.chartMaxValues) {
        chartValues.bpm.shift();
    }

    chartValues.temp.push(data.temp);
    chartValues.bpm.push(data.bpm);

    if (dashboardProperties.chartView) {
        terminal.updatePrint("chart.temp", chart.plot(chartValues.temp));
        terminal.updatePrint("chart.bpm", chart.plot(chartValues.bpm));
    }
});

socket.on("response_reconfigure", (data) => {
    endTime = performance.now();
    const elapsedTime = ((endTime - startTime) / 1000).toFixed(2);
    if (!data.success) {
        terminal.print(`Failed to reconfigure property value | Elapsed Time: ${elapsedTime}s`);
    }
    else {
        terminal.print(`Successfully reconfigured property value | Elapsed Time: ${elapsedTime}s`);
    }
});

/// Terminal Stuff
printMenu();
terminal.onLineInput(line => {
    const args = line.toLowerCase().split(' ');
    const rawArgs = line.split(' ');

    if (args[0] == "?") {
        printMenu();
    }
    else if (args[0] == "dashboard") {
        dashboardProperties.chartView = true;
        terminal.rawPrint("Chart flow mode on..");
        terminal.rawPrint("(ctrl + r) - Exit chart viewing mode");
        terminal.pauseReadLine();

        terminal.tagPrint("chart.temp.label", "Temp Chart");
        terminal.tagPrint("chart.temp", "Generating chart...");
        terminal.tagPrint("chart.bpm.label", "Beats per minute Chart");
        terminal.tagPrint("chart.bpm", "Generating chart...");
    }
    else if (args[0] == "redeploy" && args[1] != undefined) {
        try {
            const path = args[1];
            startTime = performance.now();

            terminal.print("Rebuilding contianers....");
            manager.rebuildContainer("node-device-device", path, manager.listFiles(path), () => {
                terminal.print("Container has been rebuilt...restarting them.");
                manager.restartContainers();
                
                endTime = performance.now();
                const elapsedTime = ((endTime - startTime) / 1000).toFixed(2);

                terminal.print(`Container have restarted | Elapsed Time: ${elapsedTime}s`);
            });
        } 
        catch (error) {
            terminal.rawPrint("An error has occured perhaps the path is invalid?");
            terminal.print(error);
        }
    }
    else if (args[0] == "reconfigure" && args.length == 4) {
        const targetDevice = args[1];
        const propertyName = rawArgs[2]
        const propertyValue = rawArgs[3];
        sendReconfigurationRequest(targetDevice, propertyName, propertyValue);
    }
    else {
        terminal.clearPreviousLine();
    }
});

terminal.onKeyPress((event) => {
    //ctrl + r
    if ((event.ctrlKey && event.rawcode == 82) && dashboardProperties.chartView) {
        dashboardProperties.chartView = false;
        terminal.clearTagPrint("chart.temp");
        terminal.clearTagPrint("chart.bpm");
        terminal.clearTagPrint("chart.temp.label");
        terminal.clearTagPrint("chart.bpm.label");
        terminal.print("Chart flow mode is now turned off...");
        terminal.resumeReadLine();
    }
});