var Docker = require('dockerode');
const fs = require('fs');
var docker = new Docker();

module.exports = {
    listFiles: function (folder) {
        var files = [];
        fs.readdirSync(folder).forEach(file => {
            files.push(file);
        });
        return files;
    },

    rebuildContainer: function (name, path, files, callback) {
        docker.buildImage({
            context: path,
            src: files
        }, { t: name }, function (err, response) {
            callback();
        });
    },

    restartContainers: function() {
        docker.listContainers(function (err, containers) {
            containers.forEach(function (containerInfo) {
              docker.getContainer(containerInfo.Id).restart();
            });
        });
    }
};