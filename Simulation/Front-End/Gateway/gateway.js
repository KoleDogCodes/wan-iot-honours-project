//Import Libraires 
var app = require('express')();
const cors = require('cors');
const server = require('http').Server(app);
const io = require('socket.io')(server);
const backendIo = require("socket.io-client");
const terminalUtili = require('./terminal-utili.js');
const port = 80;
const gatewayId = getPsudeoRandom(3);
const backendSocket = backendIo("https://iot-backend-server.herokuapp.com/", { query: `deviceId=${gatewayId}&type=GATEWAY` });
const terminal = require('./terminal-utili.js');
var deviceList = [];
var deviceProperties = {
    debug: false,
};

app.use(cors());
server.listen(port);

function getPsudeoRandom(length) {
    return parseInt(Date.now().toString().substring(Date.now().toString().length - length));
}

function printMenu() {
    terminal.rawPrint(`------ MENU (PORT=${port}) ------`);
    terminal.rawPrint("/device list - List all connected devices");
    terminal.rawPrint("/device ping <id> - Ping a device");
    terminal.print("/device debug - Display incoming messages from all devices");
}

function enableGateway() {
    backendSocket.emit("add_gateway", {
        gatewayId: gatewayId,
        address: `http://localhost:${port}`
    });
}

function getDevice(id) {
    var foundDevice = undefined;

    deviceList.forEach(device => {
        if (device.id == id) {
            foundDevice = device;
        }
    });

    return foundDevice;
}

function removeDevice(id) {
    const device = getDevice(id);
    const idx = deviceList.indexOf(device);
    deviceList.splice(idx, 1);
}

///Events from backend server
backendSocket.on("request_device_reconfigure", function (data) {
    terminal.rawPrint("Received IoT device reconfiguration request...");

    deviceList.forEach(device => {
        if (device.id == data.deviceId) {
            io.to(device.socketId).emit('request_device_reconfigure', data);
            terminal.print("Forwarding IoT device reconfiguration request...");
            return;
        }
    });
});

backendSocket.on("request_gateway_reconfigure", function (data) {
    if (!deviceProperties.hasOwnProperty(data.key)) {
        backendSocket.emit("response_reconfigure", {success: false});
        return;
    }

    deviceProperties[data.key] = data.value;
    backendSocket.emit("response_reconfigure", {success: true});
});

/// Events for the gateway
io.on("connection", function (socket) {
    socket.on('ping_gateway', function (data) {
        const deviceId = socket.handshake.query.deviceId;
        const type = socket.handshake.query.type;

        if (type == "IOT-DEVICE" && getDevice(deviceId) == undefined) {
            deviceList.push({ id: deviceId, socketId: socket.id });
        }

        terminal.print(data);

        socket.emit("ping_gateway", data);
    });

    socket.on('device_data_stream', function (data) {
        const deviceId = socket.handshake.query.deviceId;
        const type = socket.handshake.query.type;

        if (deviceProperties.debug) {
            terminal.print(`Device "${deviceId}" said: ${JSON.stringify(data)}`);
        }

        backendSocket.emit("device_data_stream", data);
    });

    socket.on('response_reconfigure', function (data) {
        backendSocket.emit("response_reconfigure", data);
    });

    socket.on('disconnect', function () {
        const deviceId = socket.handshake.query.deviceId;
        const type = socket.handshake.query.type;

        if (type == "IOT-DEVICE") {
            removeDevice(deviceId);
            terminal.print(`IoT device "${deviceId}" has disconnected.`);

        }
    });
});

/// Terminal Stuff
enableGateway();
printMenu();
terminal.onLineInput(line => {
    const args = line.toLowerCase().split(' ');

    if (args[0] == "?") {
        printMenu();
    }
    else if (args[0] == "device" && args[1] == "list") {
        if (deviceList.length == 0) {
            terminal.print("No connected devices.");

            return;
        }

        const tableHeaders = ['Device ID', 'Socket Connection ID'];
        const columnWidths = [25, 50];
        const tableValues = [];

        deviceList.forEach(element => {
            var row = [];
            row.push(element.id);
            row.push(element.socketId);
            tableValues.push(row);
        });

        terminal.tagPrint("device.list", terminal.generateTable(tableHeaders, columnWidths, tableValues));

    }
    else if (args[0] == "device" && args[1] == "ping" && args[2] != undefined) {
        const device = getDevice(args[2]);

        if (device == undefined) {
            terminal.print(`Device "${args[2]}" is not connected to this gateway`);

            return;
        }

        io.to(device.socketId).emit('ping_gateway', `Device "${device.id}" has been pinged by gateway "${gatewayId}".`);
        terminal.print(`Pinging device "${device.id}"...please check device terminal window.`);
    }
    else if (args[0] == "device" && args[1] == "debug") {
        deviceProperties.debug = true;
        terminal.rawPrint("Debug mode is now turned on.");
        terminal.print("(ctrl + r) - Exit debug mode");
        terminal.pauseReadLine();
    }
    else {
        terminal.clearPreviousLine();
    }
});
