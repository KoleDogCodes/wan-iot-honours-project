//Constraints 
const minTemp = -1;
const maxTemp = 40;
const minBpm = 40;
const maxBmp = 150;

//Filter variables
const maxValues = 10;
var tempFilter = [];
var bmpFilter = [];

function fetchTemp() {
    const fetchedValue = Math.random() * (maxTemp - minTemp) + minTemp;
    var smoothedValue = 0;

    if (tempFilter.length < maxValues) {
        tempFilter.push(fetchedValue);
    }
    else {
        tempFilter.shift();
        tempFilter.push(fetchedValue);
    }

    tempFilter.forEach(temp => {
        smoothedValue += temp;
    });

    return Math.round(smoothedValue / tempFilter.length, 2);
}

function fetchBmp() {
    const fetchedValue = Math.floor(Math.random() * (maxBmp - minBpm + 1)) + minBpm;
    var smoothedValue = 0;

    if (bmpFilter.length < maxValues) {
        bmpFilter.push(fetchedValue);
    }
    else {
        bmpFilter.shift();
        bmpFilter.push(fetchedValue);
    }

    bmpFilter.forEach(bmp => {
        smoothedValue += bmp;
    });

    return Math.round(smoothedValue / bmpFilter.length);
}

module.exports = {
    fetchSensorData: function() {
        const data = {
            temp: fetchTemp(),
            bpm: fetchBmp()
        };

        return data;
    }
}