const deviceId = Date.now().toString().substring(Date.now().toString().length - 5);
const io = require("socket.io-client");
const gatewayIo = require("socket.io-client");
const terminal = require('./terminal-utili.js');
const sensor = require('./simulate-data.js');
var gatewaySocket;
var socket = io("https://iot-backend-server.herokuapp.com/", {
    query: `deviceId=${deviceId}&type=IOT-DEVICE`
});
var deviceProperties = {
    idleTimeout: 1000,
    sensorTimer: 5000,
    version: 0.1,
    autonomous: true
};

function printMenu() {
    terminal.rawPrint(`------ MENU v${deviceProperties.version}------`);
    terminal.rawPrint("/gateway lists - List nearby gateways");
    terminal.rawPrint("/gateway connect <id> - Connect to a gateway");
    terminal.print("/gateway disconnect - Disconnect from gateway");
}

function connectGateway(gatewayId) {
    socket.emit("connect_device", { gatewayId: gatewayId });
}

function disconnectGateway() {
    gatewaySocket.disconnect();
    gatewaySocket = undefined;
    terminal.print("Disconnected from gateway.");
}

function startSensorSimulatiuon() {
    const intervalId = setInterval(() => {
            if (gatewaySocket == undefined) {
                clearInterval(intervalId);
                return;
            }

            var data = sensor.fetchSensorData();
            data.deviceId = deviceId;
            gatewaySocket.emit("device_data_stream", data);
        },
        deviceProperties.sensorTimer);

    return intervalId;
}

/// Backend Events
socket.on("list_gateway", (data) => {
    const gateways = data;
    const tableHeaders = ['Gateway ID', 'Gateway Address'];
    const columnWidths = [25, 50];
    const tableValues = [];

    gateways.forEach(element => {
        var row = [];
        row.push(element.id);
        row.push(element.address);
        tableValues.push(row);
    });

    terminal.tagPrint("gateway.list", terminal.generateTable(tableHeaders, columnWidths, tableValues));

    //Automatically connect to first avialble gateway
    if (deviceProperties.autonomous && gateways.length > 0) {
        connectGateway(gateways[0].id);
    }
});

socket.on("connect_device", (data) => {
    if (typeof(data) != "object") {
        terminal.print(data);
        return;
    }

    const gateway = data;
    gatewaySocket = gatewayIo(gateway.address, {
        query: `deviceId=${deviceId}&type=IOT-DEVICE`
    });

    startGatewayEvents();
    terminal.print(`Connecting to gateway "${gateway.address}" of id '${gateway.id}'`);
    gatewaySocket.emit("ping_gateway", `Gateway "${gateway.id}" has been pinged by device "${deviceId}".`);
    startSensorSimulatiuon();
});

/// Gateway Events
function startGatewayEvents() {
    gatewaySocket.on("ping_gateway", (data) => {
        terminal.print(data);
    });

    gatewaySocket.on("request_device_reconfigure", function(data) {
        if (!deviceProperties.hasOwnProperty(data.key)) {
            gatewaySocket.emit("response_reconfigure", { success: false });
            return;
        }

        //Change type of value depening on key
        if (data.key == "sensorTimer") {
            data.value = parseInt(data.value);
        }

        gatewaySocket.emit("response_reconfigure", { success: true });
        terminal.rawPrint(`Properties modified @ ${new Date().toLocaleString()}`);
        terminal.rawPrint("------------------------------------------");
        terminal.rawPrint("Device properties has been modified (OLD)");
        terminal.print(deviceProperties);
        terminal.rawPrint("Device properties has been modified (NEW)");
        deviceProperties[data.key] = data.value;
        terminal.print(deviceProperties);

    });

    gatewaySocket.on("disconnect", () => {
        disconnectGateway();
    });
}

/// Terminal Stuff (Manual Mode)
if (deviceProperties.autonomous == false) {
    printMenu();
    terminal.onLineInput(line => {
        const args = line.toLowerCase().split(' ');

        if (args[0] == "?") {
            printMenu();
        } else if (args[0] == "gateway" && args[1] == "list") {
            socket.emit("list_gateway", null);
        } else if (args[0] == "gateway" && args[1] == "connect" && args[2] != undefined) {
            if (gatewaySocket != undefined) {
                terminal.print("You must first disconnect from the current gateway.");
                return;
            }

            connectGateway(args[2]);
        } else if (args[0] == "gateway" && args[1] == "disconnect") {
            if (gatewaySocket == undefined) {
                terminal.print("You must first connect to an active gateway.");
                return;
            }

            disconnectGateway();
        } else {
            terminal.clearPreviousLine();
        }
    });
}

//Attempt to connect to gateway 
setInterval(() => {
    if (gatewaySocket == undefined) {
        socket.emit("list_gateway", null);
    }
}, 5000);